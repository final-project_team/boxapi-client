from flask import Flask, jsonify, make_response, send_file
from commands import *

app = Flask(__name__)


# check connection (just a ping pong)
@app.route('/api/alive', methods=['GET'])
def get_alive():
    return jsonify({'status': 'OK'})


# return box uptime
@app.route('/api/uptime', methods=['GET'])
def get_up_time():
    return jsonify({'uptime': get_uptime()})


@app.route('/api/backup', methods=['GET'])
def get_conf_backup_file():
    backup = do_backup()
    return send_file(backup, as_attachment=True)


# Add new IoT device
@app.route('/api/new_device', methods=['GET'])
def new_device():
    new_entry = {'error': 'Maximum devices reached ({})'.format(len(db))}
    if len(db) < 7:
        ran_id = random_id()
        new_entry = {'id': ran_id,
                     'ssid': 'iot_wifi_{}'.format(ran_id),
                     'key': get_wifi_key(),
                     'subnet_num': random_sub_num()}
        open_new_wifi_iface(new_entry['ssid'], new_entry['key'], ran_id, new_entry['subnet_num'])
        db.insert(new_entry)
    return jsonify(new_entry)


# Remove IoT device
@app.route('/api/remove_device/<string:net_id>', methods=['GET'])
def remove_device(net_id):
    q = Query()
    ssid = 'N/A'
    if len(db.search(q.id == net_id)) > 0:
        ssid = db.search(q.id == net_id)[0]['ssid']
        delete_device(net_id)
        db.remove(q.id == net_id)
    return jsonify({'SSID': ssid})


# Check device connection
@app.route('/api/check_device/id/<string:net_id>', methods=['GET'])
def check_device_connection(net_id):
    wifi_net = Query()
    ssid = db.search(wifi_net.id == net_id)[0]['ssid']
    iface = ssid_to_interface(ssid)
    mac = get_the_mac(iface)
    return jsonify({'SSID': ssid,
                    'iface': iface,
                    'MAC': mac})


# Check signal for device
@app.route('/api/check_signal/id/<string:net_id>', methods=['GET'])
def check_signal(net_id):
    try:
        wifi_net = Query()
        ssid = db.search(wifi_net.id == net_id)[0]['ssid']
        iface = ssid_to_interface(ssid)
        quality = get_quality_by_iface(iface)
        return jsonify({'signal': quality})
    except IndexError:
        return jsonify({'signal': "N/A"})


# Check device connection
@app.route('/api/check_devices', methods=['GET'])
def check_devices():
    wifi_net = Query()
    dev_arr = []
    devices = db.search(wifi_net.id != 1)
    for dev in devices:
        iface = ssid_to_interface(dev['ssid'])
        if not iface:
            pass
        mac = get_the_mac(iface)
        dev_arr.append({'id': dev['id'],
                        'iface': iface,
                        'mac': mac})
    return jsonify(dev_arr)


# Device health check (still alive? get signal)
@app.route('/api/curr_networks', methods=['GET'])
def curr_net():
    all_devices = Query()
    all = db.search(all_devices.id != 1)
    return jsonify(all)


# Get DNS log from dnsmasq log
@app.route('/api/get_dns_log', methods=['GET'])
def dns_log_req():
    return jsonify(get_dns_log())


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=88, use_reloader=False)
