import requests
from scapy.all import IP, sniff, TCP
import os
from tinydb import TinyDB, Query
from datetime import datetime
import re
API_KEY = "546337114253"
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
db = TinyDB(os.path.join(ROOT_DIR, 'db.json'))
query = Query()
devices = db.all()


connections = {}
regex = '192\.168\.(.*)\.(.*)'


def process_packet(pkt):
    try:
        if TCP in pkt and pkt[TCP].flags == 2:
            device = db.search(query.subnet_num == re.search(regex, pkt[IP].src).group(1))
            if device:
                device_id = device[0]['id']
                connection = {
                    "device_id": device_id,
                    "ip": pkt[IP].dst,
                    "port": pkt[TCP].dport,
                    "timestamp": datetime.now().isoformat()
                }
                try:
                    r = requests.post('http://10.8.0.1:3000/api/log_connection?key={key}'.format(key=API_KEY),
                                      json=connection)
                except requests.exceptions.ConnectionError:
                    print "Connection refused"
                print('SYN packet detected port: ' + str(pkt[TCP].dport) + ' to IP Dst: ' + pkt[IP].dst)
    except AttributeError as e:
        pass


sniff(prn=process_packet, filter="tcp[0xd]&18=2")
