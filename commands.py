"""
 here we'll have all the uci commands
 to make things work on the box
"""
import requests
import uptime
import time
import os
import openwrt
import string
import random
from datetime import datetime, timedelta
import dateutil.parser as date_parser
from subprocess import call, Popen, STDOUT, check_output, CalledProcessError
from tinydb import TinyDB, where, Query
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
import re
from collections import defaultdict

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
API_KEY = "546337114253"
db = TinyDB(os.path.join(ROOT_DIR, 'db.json'))
manager = openwrt.create_manager(hostname='localhost', username='root', password='awsaws')
glob_tx_dic = {}

# get the uptime
def get_uptime():
    sec = timedelta(seconds=uptime.uptime())
    d = datetime(1, 1, 1) + sec
    return "%d days, %d hours" % (d.day - 1, d.hour)


def open_new_wifi_iface(ssid, wifi_key, wifi_id, subnet_num):
    # WiFi setup
    # Popen(['uci', 'add', 'wireless', 'wifi-iface'])
    Popen(['uci', 'set', 'wireless.{wifi_id}=wifi-iface'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.device=radio0'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.encryption=psk2'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.mode=ap'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.network={lan}'.format(wifi_id=wifi_id, lan=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.ssid={ssid}'.format(ssid=ssid, wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.key={key}'.format(key=wifi_key, wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.maxassoc=1'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.hidden=0'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'wireless.{wifi_id}.disabled=0'.format(wifi_id=wifi_id)])

    # VLAN setup
    Popen(['uci', 'set', 'network.{wifi_id}=interface'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'network.{wifi_id}.force_link=1'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'network.{wifi_id}.proto=static'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'network.{wifi_id}.netmask=255.255.255.0'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'network.{wifi_id}.type=bridge'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'network.{wifi_id}.ipaddr=192.168.{subnet_num}.1'.format(wifi_id=wifi_id,
                                                                                  subnet_num=subnet_num)])
    Popen(['uci', 'set', 'network.{wifi_id}.gateway=192.168.{subnet_num}.1'.format(wifi_id=wifi_id,
                                                                                   subnet_num=subnet_num)])
    Popen(['uci', 'set', 'network.{wifi_id}.broadcast=192.168.{subnet_num}.255'.format(wifi_id=wifi_id,
                                                                                       subnet_num=subnet_num)])

    # DHCP Server setup
    Popen(['uci', 'set', 'dhcp.{wifi_id}=dhcp'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.interface={wifi_id}'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.start=100'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.limit=1'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.leasetime=12h'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.dhcpv6=server'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.ra=server'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.ra_management=1'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.force=1'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'dhcp.{wifi_id}.networkid=eth0.{subnet_num}'.format(wifi_id=wifi_id, subnet_num=subnet_num)])

    # Firewall zone setup
    Popen(['uci', 'set', 'firewall.{wifi_id}=zone'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.input=ACCEPT'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.forward=REJECT'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.output=ACCEPT'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.name={wifi_id}'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.network={wifi_id}'.format(wifi_id=wifi_id)])
    Popen(['uci', 'set', 'firewall.{wifi_id}.masq=1'.format(wifi_id=wifi_id)])

    # Zone forwarding setup
    Popen(['uci', 'add', 'firewall', 'forwarding'])
    Popen(['uci', 'set', 'firewall.@forwarding[-1]=forwarding'])
    Popen(['uci', 'set', 'firewall.@forwarding[-1].dest=vpn'])
    Popen(['uci', 'set', 'firewall.@forwarding[-1].src={wifi_id}'.format(wifi_id=wifi_id)])

    # commit changes and restart WiFi
    Popen(['uci', 'commit'])
    Popen(['wifi'])
    return


def delete_device(wifi_id):
    print "::delete:: "
    print wifi_id
    # delete WiFi AP
    Popen(['uci', 'delete', 'wireless.{wifi_id}'.format(wifi_id=wifi_id)])
    # delete VLAN interface
    Popen(['uci', 'delete', 'network.{wifi_id}'.format(wifi_id=wifi_id)])
    # delete dhcp config
    Popen(['uci', 'delete', 'dhcp.{wifi_id}'.format(wifi_id=wifi_id)])
    # delete firewall zone
    Popen(['uci', 'delete', 'firewall.{wifi_id}'.format(wifi_id=wifi_id)])
    # delete zone forwarding to vpn
    fire_data = check_output(['uci', 'show', 'firewall']).split('\n')
    num = None
    for line in fire_data:
        if 'src={}'.format(wifi_id) in line:
            num = line.split('[')[1][0]
    if num is not None:
        Popen(['uci', 'delete', 'firewall.@forwarding[{n}]'.format(n=num)])
    Popen(['uci', 'commit'])
    Popen(['wifi'])


def do_backup():
    [os.remove(f) for f in os.listdir(os.getcwd()) if f.startswith("config")]
    backup_name = 'config-backup-{}.tar.gz'.format(int(time.time()))
    open(backup_name, mode='wb').write(manager.backup.config_dir())
    return backup_name


def get_wifi_key():
    chars = string.ascii_letters + string.digits
    return ''.join(random.choice(chars) for _ in range(8))


def random_id():
    ran = random.choice(string.ascii_uppercase) + random.choice(string.digits)
    while db.search(Query().ssid == 'iot_wifi_{}'.format(ran)):
        ran = random.choice(string.ascii_uppercase) + random.choice(string.digits)
    return ran


def random_sub_num():
    ran = str(random.randint(10, 100))
    while db.search(Query().subnet_num == ran):
        ran = str(random.randint(10, 100))
    return ran


def ssid_to_interface(ssid):
    try:
        raw_iface = check_output("iw dev | grep -e {ssid} -B 4 | grep Interface".format(ssid=str(ssid)),
                                 shell=True, stderr=STDOUT)
        iface = raw_iface.split(" ")[1].rstrip()
        return iface
    except CalledProcessError:
        return None


def get_the_mac(iface):
    try:
        mac = check_output("iw dev {iface} station dump | grep Station".format(iface=str(iface)),
                           shell=True, stderr=STDOUT).split(" ")[1]
        if mac == 'No':
            mac = 'No station connected'
    except CalledProcessError:
        mac = 'No station connected'
    return mac


def get_quality_by_iface(iface):
    try:
        signal = check_output("iw dev {iface} station dump | grep signal:".format(iface=str(iface)),
                              shell=True, stderr=STDOUT).strip().replace('\t', '').split()[1]
        quality = 2 * (int(signal) + 100)
        if quality > 100:
            quality = 100
        elif quality < 0:
            quality = 0
    except CalledProcessError:
        quality = "N/A"
    return quality


def get_current_stats(t, iface):
    with open('/sys/class/net/' + iface + '/statistics/tx_' + t, 'r') as f:
        data = f.read()
    return int(data.strip())


def stats_update():
    global glob_tx_dic
    tx_dic = {}
    all_devices = Query()
    _all = db.search(all_devices.id != 1)
    for device in _all:
        tx_bytes = get_current_stats('bytes', "br-" + device['id'])
        tx_pkt = get_current_stats('packets', "br-" + device['id'])
        if device['id'] in glob_tx_dic:
            tx_dic[device['id']] = {
                'tx_bytes': (tx_bytes - glob_tx_dic[device['id']]['tx_bytes']),
                'tx_packets': (tx_pkt - glob_tx_dic[device['id']]['tx_packets']),
                'timestamp': datetime.now().isoformat()
            }
        else:
            tx_dic[device['id']] = {
                'tx_bytes': 0,
                'tx_packets': 0,
                'timestamp': datetime.now().isoformat()
            }
        glob_tx_dic[device['id']] = {
            'tx_bytes': tx_bytes,
            'tx_packets': tx_pkt
        }
    r = requests.post('http://10.8.0.1:3000/api/stats_update?key={key}'.format(key=API_KEY), json=tx_dic)
    print r.text
    print tx_dic


def get_dns_log():
    def parseline(line):
        m = re.search('(.*) dnsmasq\[(\d*)\]: (\d*) (.*) (.*) (.*) (from|is) (.*)', line)
        try:
            r = {
                'ts': date_parser.parse(m.group(1)).isoformat(),
                'query_id': m.group(3),
                'type': m.group(5),
                'domain': m.group(6),
                'source': m.group(8)
            }
        except:
            return {}
        return r

    _dnsmasq_log = "/tmp/dnsmasq.log"
    f = open(_dnsmasq_log, 'r')
    d = f.read()
    f.close()
    open(_dnsmasq_log, 'w').close()
    x = d.splitlines()
    results = {}
    for l in x:
        if 'query[A]' in l:
            q = parseline(l)
            if 'query_id' in q and q['domain'] != "IoTRouter":
                results[q['query_id']] = q
        elif 'reply' in l:
            q = parseline(l)
            if 'query_id' in q and q['query_id'] in results:
                results[q['query_id']]['ip'] = q['source']

    my_db = Query()
    devices = db.search(my_db.id != 1)
    sub_num_to_id = {}
    for device in devices:
        sub_num_to_id[str(device['subnet_num'].decode())] = str(device['id'])
    results_per_device = defaultdict(list)
    regex = "(\d*).(\d*).(\d*).(\d*)"
    for res in results.itervalues():
        sub_n = re.search(regex, res['source']).group(3)
        if sub_n in sub_num_to_id:
            results_per_device[sub_num_to_id[sub_n]].append(res)
    return results_per_device


scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
    func=stats_update,
    trigger=IntervalTrigger(seconds=5),
    id='traffic monitor',
    name='send tx bytes and packets every 5 seconds',
    replace_existing=True)
# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())
