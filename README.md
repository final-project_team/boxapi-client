Box API
=======

This is the code for the IoT router, placed behind NAT (home LAN). 
Each device will be connected to a dedicated WiFi AP (help isolation and management).
The box will be connected to an OpenVPN server as WAN interface and this server will manage devices and firewall setting (and more).


API
===

All API will be available under this address:
`http://<server-ip>/api`
